#encoding: utf-8

# This script is responsible to validate the creation of subtasks
# How can be saw in code below it is necessary that user be logged to do interations
# It is required a task already created


require_relative 'HomePage'


Given(/^user should see a button labeled as ‘Manage Subtasks’$/) do
  @user = 'nathanael.araujos@gmail.com'
  @pwd = "nathan1234567"
  @browser = Selenium::WebDriver.for :firefox
  @toDoApp = MyTasks.new(@browser)
  @toDoApp.navigateToApp('http://qa-test.avenuecode.com/')
  @toDoApp.navigateToMyTasks()
  @toDoApp.login(@user, @pwd)
end

Then(/^This button should have the number of subtasks created for that tasks$/) do
  @task = "New Task"
  @toDoApp.addingNewTaskByHittingEnter(@task)
  @browser.navigate.refresh
  expect(@toDoApp.verifyManageSubTasks).to eq("true")
  @browser.quit
end

Given(/^Clicking this button opens up a modal dialog$/) do
  @otherTask  = "TASK"
  @toDoApp.addingNewTaskByHittingEnter(@otherTask)
  @browser.navigate.refresh
  @browser.manage.timeouts.implicit_wait = 3
  @toDoApp.navigateToSubTasks()
end

Then(/^This popup should have a read only field with the task ID and the task description$/) do
  @status = @toDoApp.modifingField() ? "false" : "true"
  expect(@status).to eq("true")
end

Given(/^There should be a form so you can enter the SubTask Description \((\d+) characters\)$/) do |len|
  @subTask = "1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890qwer"
  @toDoApp.addinngSubTask(@subTask)
  @status = @subTask.length > len.to_i ? "false" : "true"
  expect(@status).to eq("true")
  @browser.quit
end

Given(/^There should be a form so you can enter SubTask due date \(MM\/dd\/yyyy format\)$/) do
  @dueDate = "0211216"
  @status = "/[0-9][0-9]/[9-9][0-9]/[0-9][0-9][0-9][0-9]/".match(@dueDate) ? "true" : "false"
  @toDoApp.addingDueDate(@dueDate)
  expect(@status).to eq("true")
  @browser.quit
end

Then(/^The user should click on the add button to add a new Subtask and the Task Description and Due Date are required fields$/) do
  @status = @toDoApp.addinngSubTask(@subTask) ? "false" : "true"
  expect(@status).to eq("true")
  @browser.quit
end

Then(/^Subtasks that were added should be appended on the bottom part of the modaldialog$/) do
  @toDoApp.sendingSubTask(@subTask,@dueDate)
  @browser.quit
end
