#encoding: utf-8

# This script is responsible to validate the creation of tasks
# How can be saw in code below it is necessary that user be logged to do interations

require_relative 'HomePage'



Given(/^the user should always see the "([^"]*)" link on the NavBar$/) do |linkTasks|
  @browser = Selenium::WebDriver.for :firefox
  @toDoApp = MyTasks.new(@browser)
  @toDoApp.navigateToApp('http://qa-test.avenuecode.com/')
  @linkText = @browser.find_element(:link_text , linkTasks).text
  expect(@linkText).to eq(linkTasks)
end

Given(/^Clicking this link will redirect the user to a page with all the created tasks so far$/) do
  @user = 'nathanael.araujos@gmail.com'
  @pwd = "nathan1234567"
  @toDoApp.navigateToMyTasks()
  @toDoApp.login(@user, @pwd)
end

Then(/^the user should see a message on the top part saying that list belongs to the logged user: if the logged user name is NathanaelSilva, then the displayed message should be "([^"]*)"$/) do |arg1|
  @alertText = @browser.find_element(:css , '.alert.alert-info').text
  expect(@alertText).to eq(message)
  @browser.quit
end

Then(/^the user should be able to enter a new task by hitting enter or clicking on the add task button\.$/) do
  @task1 = "By Hitting Enter"
  @task2 = "Clicking on Add Button"
  @toDoApp.addingNewTaskByHittingEnter(@task1)
  #@toDoApp.addingNewTaskByClickonButton(@task2)
  @browser.quit
end

Then(/^the task should require at least (\d+) characters so the user can enter it\.$/) do |len|
  @task = "ab"
  @toDoApp.addingNewTaskByHittingEnter(@task)
  @status = @task.length < len.to_i ? "false" : "true"
  expect(@status).to eq("true")
  @browser.quit

end

Then(/^the task can’t have more than (\d+) characters\.$/) do |len|
  @task = "1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890qwer"
  @toDoApp.addingNewTaskByHittingEnter(@task)
  @status = @task.length > len.to_i ? "false" : "true"
  expect(@status).to eq("true")
  @browser.quit
end

Then(/^When added, the task should be appended on the list of created tasks\.$/) do
  @task = "Nathan"
  @toDoApp.addingNewTaskByHittingEnter(@task)
  @browser.quit
end
