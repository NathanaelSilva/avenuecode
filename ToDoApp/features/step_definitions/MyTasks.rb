#encoding: utf-8

#This class has all funcitons about user interations with task management

class MyTasks < HomePage

  def initialize(driver)
    @@browser = driver
  end

  def navigateToMyTasks()
    @@browser.find_element(:link_text, 'My Tasks').click
  end

  def addingNewTaskByHittingEnter(task)
    @@browser.find_element(:id, 'new_task').send_keys(task)
    @@browser.find_element(:id, 'new_task').send_keys :enter
  end

  def verifyManageSubTasks()
    if(@@browser.find_element(:xpath , "//button[text()='(0) Manage Subtasks']"))
      @status = "true"
    else
      @status = "false"
    end

    return @status
  end

  def navigateToSubTasks()
    @@browser.find_element(:xpath , "//button[text()='(0) Manage Subtasks']").click
  end

  def removeTasks()
    @@browser.find_element(:xpath , "//button[text()='remover']").click
  end

  def addingDueDate(dueDate)
    @@browser.find_element(:id, 'dueDate').send_keys(dueDate)
    @@browser.find_element(:id, 'dueDate').send_keys :enter
  end

  def addinngSubTask(subTask)
    @@browser.find_element(:id, 'new_sub_task').send_keys(subTask)
    @@browser.find_element(:id, 'new_sub_task').send_keys :enter
  end

  def sendingSubTask(subTask, dueDate)
    @@browser.find_element(:id, 'new_sub_task').send_keys(subTask)
    @@browser.find_element(:id, 'dueDate').send_keys(dueDate)
    @@browser.find_element(:id, 'new_sub_task').send_keys :enter

  end

  def modifingField()
    @@browser.find_element(:id, 'edit_task').send_keys('Modifing')
  end


#  def addingNewTaskByClickonButton(task)
#
#  end


end
