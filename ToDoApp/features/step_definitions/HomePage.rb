#encoding: utf-8

#This class has all functions about navegation on website and login

class HomePage

  @@browser = nil

  def initialize(driver)
    @@browser = driver
  end

  def navigateToApp(url)
    @@browser.get(url)
    @@browser.manage.window.maximize
    @@browser.manage.timeouts.implicit_wait = 7
  end

  def login(user, pwd)
      @@browser.find_element(:id, 'user_email').send_keys(user)
      @@browser.find_element(:id, 'user_password').send_keys(pwd)
      @@browser.find_element(:name , 'commit').click
  end

end
