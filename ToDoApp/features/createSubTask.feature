Feature: Create SubTask
  As a ToDo App user
  I should be able to create a subtask
  So I can break down my tasks in smaller pieces

    Scenario: Number of subtasks created
    Given  user should see a button labeled as ‘Manage Subtasks’
      Then This button should have the number of subtasks created for that tasks

    Scenario: Popup fields
      Given  user should see a button labeled as ‘Manage Subtasks’
      Given Clicking this button opens up a modal dialog
        Then This popup should have a read only field with the task ID and the task description

    Scenario: Length Subtask Description
      Given  user should see a button labeled as ‘Manage Subtasks’
      Given Clicking this button opens up a modal dialog
      Given There should be a form so you can enter the SubTask Description (250 characters)

    Scenario: Due Date Format
      Given  user should see a button labeled as ‘Manage Subtasks’
      Given Clicking this button opens up a modal dialog
      Given There should be a form so you can enter SubTask due date (MM/dd/yyyy format)

    Scenario: Required fields
      Given  user should see a button labeled as ‘Manage Subtasks’
      Given Clicking this button opens up a modal dialog
        Then The user should click on the add button to add a new Subtask and the Task Description and Due Date are required fields

    Scenario: Appended on the bottom part of the modal dialog
      Given  user should see a button labeled as ‘Manage Subtasks’
      Given Clicking this button opens up a modal dialog
        Then Subtasks that were added should be appended on the bottom part of the modaldialog
