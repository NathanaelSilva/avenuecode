Feature: Create Task

  As a ToDo App user
  I should be able to create a task
  So I can manage my tasks

  Scenario: Displaying message of all tasks
    Given the user should always see the "My Tasks" link on the NavBar
      And Clicking this link will redirect the user to a page with all the created tasks so far
      Then the user should see a message on the top part saying that list belongs to the logged user: if the logged user name is NathanaelSilva, then the displayed message should be "Hey NathanaelSilva, this is your todo list for today:"

  Scenario: Able to enter a new task by hitting enter or clicking on button
    Given the user should always see the "My Tasks" link on the NavBar
      And Clicking this link will redirect the user to a page with all the created tasks so far
      Then the user should be able to enter a new task by hitting enter or clicking on the add task button.

  Scenario: Requirement about task length (least 03 characters)
  Given the user should always see the "My Tasks" link on the NavBar
      And Clicking this link will redirect the user to a page with all the created tasks so far
      Then the task should require at least 03 characters so the user can enter it.

  Scenario: Requirement about task length (more 250 characters)
    Given the user should always see the "My Tasks" link on the NavBar
      And Clicking this link will redirect the user to a page with all the created tasks so far
      Then the task can’t have more than 250 characters.

  Scenario: Appending tasks on the list of created tasks.
    Given the user should always see the "My Tasks" link on the NavBar
      And Clicking this link will redirect the user to a page with all the created tasks so far
      Then When added, the task should be appended on the list of created tasks.
